(setq inhibit-startup-message t)

(scroll-bar-mode -1)        ; Disable visible scrollbar
(tool-bar-mode -1)          ; Disable the toolbar
(tooltip-mode -1)           ; Disable tooltips
(set-fringe-mode 10)        ; Give some breathing room (left/right margins)

(setq make-backup-files nil) ;; do not make backup files

(menu-bar-mode -1)          ; Disable the menu bar

;; Make ESC quit prompts
(global-set-key (kbd "<escape>") 'keyboard-escape-quit)

;; config line numbers
(column-number-mode)
(global-display-line-numbers-mode t)
;; disable line number for given buffers
(dolist (mode '(org-mode-hook
		term-mode-hook
		eshell-mode-hook))
  (add-hook mode (lambda() (display-line-numbers-mode 0))))

;; Initialize package sources
(require 'package)

(setq package-archives '(("melpa" . "https://melpa.org/packages/")
                         ("org" . "https://orgmode.org/elpa/")
                         ("elpa" . "https://elpa.gnu.org/packages/")))
(package-initialize)
(unless package-archive-contents
 (package-refresh-contents))

;; Initialize use-package
(unless (package-installed-p 'use-package)
   (package-install 'use-package))

(require 'use-package)
(setq use-package-always-ensure t)
;; set theme
(use-package doom-themes
  :init (load-theme 'doom-nord t))

;;powerline-like status bar
(use-package doom-modeline
  :init (doom-modeline-mode 1))

(use-package all-the-icons)

;; Better minibuffer with descriptions and shortcuts
(use-package ivy
  :diminish
  :bind (("C-s" . swiper)                 ;; better search
         :map ivy-minibuffer-map
         ("TAB" . ivy-alt-done)	
         ("C-j" . ivy-next-line)          ;; use C-j for moving
         ("C-k" . ivy-previous-line)      ;; use C-k for moving
         :map ivy-switch-buffer-map
         ("C-k" . ivy-previous-line)      ;; use C-j/k for moving
         ("C-d" . ivy-switch-buffer-kill) ;; close buffers while showing them
	 )
  :init (ivy-mode 1))

(use-package ivy-rich
  :init (ivy-rich-mode 1))

(use-package counsel
  :bind (("M-x" . counsel-M-x)
	 ("C-x f" . counsel-find-file)
	 )
  :config (setq ivy-initial-inputs-alist nil)) ;; dont start searches with ^ 

(use-package rainbow-delimiters
  :hook 'prog-mode-hook . 'rainbow-delimiters-mode)

;; shows help for commands / keys
(use-package which-key
  :init (which-key-mode)
  :diminish which-key-mode
  :config (setq which-key-idle-delay 0.3))

;; better help/man documentation
(use-package helpful
  :custom
  (counsel-describe-function-function #'helpful-callable)
  (counsel-describe-variable-function #'helpful-variable)
  :bind
  ([remap describe-function] . counsel-describe-function)
  ([remap describe-command] . helpful-command)
  ([remap describe-variable] . counsel-describe-variable)
  ([remap describe-key] . helpful-key))

;; do not enable evil mode on some buffers
(defun rey/evil-hook ()
  (dolist (mode '(org-mode
		  custom-mode
                  eshell-mode
                  term-mode))
  (add-to-list 'evil-emacs-state-modes mode)))

;; vim bindings ;)
(use-package evil
  :init
;;   (setq evil-want-integration t)
;;   (setq evil-want-keybinding nil)
;;   (setq evil-want-C-u-scroll t)
;;   (setq evil-want-C-i-jump nil)
;;   (setq evil-respect-visual-line-mode t)
  :config
  (add-hook 'evil-mode-hook 'rey/evil-hook)
  (evil-mode 1)
  (define-key evil-insert-state-map (kbd "C-g") 'evil-normal-state)
;;   (define-key evil-insert-state-map (kbd "C-h") 'evil-delete-backward-char-and-join)
  
  (evil-set-initial-state 'messages-buffer-mode 'emacs)
  (evil-set-initial-state 'dashboard-mode 'emacs))

;; org mode
(use-package org
  :pin org  ;; chose org repo over melpa
  :config
  (setq org-agenda-start-with-log-mode t)
  (setq org-log-done 'time) ;; add timestamp when task state chage to done
  (setq org-log-into-drawer t)

  (setq org-agenda-files
        '("~/Documents/OrgFiles/Tasks.org"
          "~/Documents/OrgFiles/Habits.org"
          "~/Documents/OrgFiles/Birthdays.org"))
  )

;; C++ setup
(use-package company
  :ensure t
  :hook (prog-mode . company-mode)
  :custom
  (company-backends '(company-capf)))

(use-package yasnippet)

(use-package lsp-mode
  :ensure t
  :hook ((prog-mode . lsp-deferred)
	 (c++-mode . lsp)
	 (c++-mode . company-mode)
	 (c++-mode . yas-minor-mode)
	 (lsp-mode . lsp-enable-which-key-integration)
	 )
  :custom
  (lsp-prefer-capf t)
  (lsp-auto-guess-root t)
  (lsp-keep-workspace-alive nil))
;(add-hook 'c++-mode-hook 'lsp)
;(add-hook 'c++-mode-hook 'company-mode)
;(add-hook 'c++-mode-hook 'yas-minor-mode)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   '(lsp-mode company yasnippet which-key use-package rainbow-delimiters org-bullets ivy-rich helpful evil doom-themes doom-modeline counsel command-log-mode)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
